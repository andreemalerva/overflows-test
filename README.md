# Acerca de mí
¡Hola! 

Soy Andree, estoy creando mi portafolio en [andreemalerva.com](http://www.andreemalerva.com/), actualmente soy Desarrollador Front End, y me agrada seguir aprendiendo.

Trabajemos juntos, te dejo por acá mi contacto.
```
📩 hola@andreemalerva.com
📲 +52 228 353 0727
```

# Acerca del proyecto

Este es un proyecto donde se demuestra el uso de overflows en css.

Puedes visualizarlo en la siguiente liga:
[Demo for Andree Malerva](https://andreemalerva.gitlab.io/overflows-test/)🫶🏻

# Politícas de privacidad

Las imagenes, archivos css, html y adicionales son propiedad de ©2022 LYAM ANDREE CRUZ MALERVA